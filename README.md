Chcemy napisać aplikację konsolową, której jednym z argumentów piszemy dane w formacie:<br>
Nazwisko, Imię (abcd/123) +48 123 456 789<br>
oczekujemy, że aplikacja wyświetli te dane rozdzielone każde w osobnej linii<br>
<br>
out:<br>
Nazwisko<br>
Imię<br>
abcd<br>
123<br>
123456678<br>
<br>
Uwaga! Argument programu musi być jednym ciągiem (ujety w "")
