﻿using ConsoleApp1.Utils;
using ConsoleApp1.Utils.Exceptions;
using FluentAssertions;

namespace ConsoleApp1.Test.Utils
{
    public class UsersDataFormatterTest
    {
        [Theory]
        [InlineData("Nowak, Jan (abcd/123) +48 123 456 789","Nowak\nJan\nabcd\n123\n123456789")]
        [InlineData("Kowalski, Janusz (tyu/4444) +1 123456 789","Kowalski\nJanusz\ntyu\n4444\n123456789")]
        [InlineData("X, Y (a/1) +123 123456789", "X\nY\na\n1\n123456789")]
        [InlineData("X, Y (a/1) +123 1 2 3 4 5 6 7 8 9", "X\nY\na\n1\n123456789")]
        [InlineData("X, Y (a/1) +1 2", "X\nY\na\n1\n2")]
        public void Format_WhenInputIsCorrect_ShouldReturnFormattedValues(string input, string output)
        {
            var result = UsersDataFormatter.Format(input);

            result.Should().Be(output);
        }

        [Theory]
        [InlineData("X,  Y (a/1) +1 1")]
        [InlineData(" X, Y (a/1) +1 1")]
        [InlineData("X Y (a/1) +1 1")]
        [InlineData("Y (a/1) +1 1")]
        [InlineData("X, Y ( a/1) +1 1")]
        [InlineData("X, Y (a/1 ) 1")]
        [InlineData("X, Y (a/1) +1 123-123")]
        [InlineData("X, Y (a-1) +1 1")]
        [InlineData("X Y (a 1) +1 1")]
        [InlineData("X Y(a/1)+1 1")]
        [InlineData("X, Y a/1 +1 1")]
        public void Format_WhenDataHasInvalidFormat_ShouldThrowException(string invalidInput)
        {
            var action = () => UsersDataFormatter.Format(invalidInput);

            action.Should().ThrowExactly<InvalidUserDataFormatException>();
        }

        [Fact]
        public void Format_WhenInputDataTooLong_ThenShouldThrowException()
        {
            string input = "Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa, Bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb (ccccccccccccccccccccccccccccccccccccccccccccccccccccc/11111111111111111111111111111111111111111111) +22222222222222222222222222222 3333333333333333333333333333333333333333333333333333";

            var actionWithCorrectInput = () => UsersDataFormatter.Format(input);
            var actionWithIncorrectInput = () => UsersDataFormatter.Format(input + "3");

            input.Should().HaveLength(300);
            actionWithCorrectInput.Should().NotThrow();
            actionWithIncorrectInput.Should().ThrowExactly<InvalidUserDataFormatException>();
        }
    }
}
