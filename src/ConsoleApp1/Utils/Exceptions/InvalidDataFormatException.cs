﻿namespace ConsoleApp1.Utils.Exceptions
{
    public class InvalidUserDataFormatException : ApplicationException
    {
        public InvalidUserDataFormatException() : base()
        {
        }

        public InvalidUserDataFormatException(string message) : base(message)
        {
        }
    }
}
