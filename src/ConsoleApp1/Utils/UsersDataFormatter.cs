﻿using ConsoleApp1.Utils.Exceptions;
using System.Text.RegularExpressions;

namespace ConsoleApp1.Utils
{
    public static class UsersDataFormatter
    {
        private const int MAX_LENGTH = 300;
        private static Regex lastNamePattern = new Regex(@"^\w+", RegexOptions.Compiled);
        private static Regex firstNamePattern = new Regex(@"(?<=,\s)\w+", RegexOptions.Compiled);
        private static Regex departmentPattern = new Regex(@"(?<=[(])\w+", RegexOptions.Compiled);
        private static Regex departmentCodePattern = new Regex(@"(?<=[\/])\w+", RegexOptions.Compiled);
        private static Regex phonePattern = new Regex(@"(?<=[0-9]\s)[0-9\s]+$", RegexOptions.Compiled);

        /// <summary>
        /// Format users data row
        /// </summary>
        /// <param name="dataRow">User data in format: Lastname, Firstname (department/number_code) +regional_code phone_number, Max length is 300.</param>
        /// <returns>Formatted data contains Lastname, Firstname, department, departmentCode and phone number with new lines</returns>
        /// <exception cref="InvalidUserDataFormatException">Thrown if input data has incorrect format</exception>
        public static string Format(string dataRow)
        {
            if(dataRow.Length > MAX_LENGTH)
            {
                throw new InvalidUserDataFormatException();
            }

            int startPos = 0;
            string lastName = getMatch(lastNamePattern, dataRow, ref startPos);
            string firstName = getMatch(firstNamePattern, dataRow, ref startPos);
            string department = getMatch(departmentPattern, dataRow, ref startPos);
            string departmentCode = getMatch(departmentCodePattern, dataRow, ref startPos);
            string phone = getMatch(phonePattern, dataRow, ref startPos).Replace(" ", "");

            return $"{lastName}\n{firstName}\n{department}\n{departmentCode}\n{phone}";
        }

        private static string getMatch(Regex regex, string input, ref int startPos)
        {
            var match = regex.Match(input, startPos);
            if (!match.Success || string.IsNullOrWhiteSpace(match.Value))
            {
                throw new InvalidUserDataFormatException();
            }
            startPos += match.Value.Length;

            return match.Value;
        }
    }
}
