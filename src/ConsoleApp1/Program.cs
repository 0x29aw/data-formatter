﻿using ConsoleApp1.Utils;
using ConsoleApp1.Utils.Exceptions;

try
{
    if (args.Length != 1)
    {
        throw new ArgumentException("Data arg isnt specified", nameof(args));
    }
    Console.WriteLine(UsersDataFormatter.Format(args[0]));
}
catch(InvalidUserDataFormatException e)
{
    Console.WriteLine($"Invalid data format.");
}
catch(ArgumentException e)
{
    Console.WriteLine($"Argument error occured: {e.Message}");
}
catch(Exception e)
{
    Console.WriteLine("Unexpected error.");
}